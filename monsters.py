import random
import sqlite3 as lite
import subprocess as shell
import time
import datetime
import os.path #used for checking if a file exists
import getpass #for working with the password

db = lite.connect('dd.db')
cursor = db.cursor()

#clear the monster table
cursor.execute("DROP TABLE IF EXISTS monsters")

#recreate the monster table so the numbers will align properly
cursor.execute("CREATE TABLE IF NOT EXISTS monsters(ID INTEGER PRIMARY KEY AUTOINCREMENT, name text, hd integer, ac integer, attack_type integer, special1 text, special2 text, cr integer)")

#insert all the monsters into the table
cursor.execute("INSERT INTO monsters(name, hd, ac, attack_type, special1, special2, cr) VALUES ('Rat', 1, 12, 'bite', 'none', 'none', 1)")
cursor.execute("INSERT INTO monsters(name, hd, ac, attack_type, special1, special2, cr) VALUES ('Jackal', 1, 12, 'none', 'none', 'none', 1)")
cursor.execute("INSERT INTO monsters(name, hd, ac, attack_type, special1, special2, cr) VALUES ('Kobald', 1, 13, 'shortsword', 'none', 'none', 1)")
cursor.execute("INSERT INTO monsters(name, hd, ac, attack_type, special1, special2, cr) VALUES ('Goblin', 1, 12, 'none', 'none', 'none', 1)")
cursor.execute("INSERT INTO monsters(name, hd, ac, attack_type, special1, special2, cr) VALUES ('Baboon', 1, 12, 'none', 'none', 'none', 1)")
cursor.execute("INSERT INTO monsters(name, hd, ac, attack_type, special1, special2, cr) VALUES ('Giant Frog', 1, 12, 'none', 'none', 'none', 1)")

cursor.execute("INSERT INTO monsters(name, hd, ac, attack_type, special1, special2, cr) VALUES ('Giant Ant', 2, 16, 'shortsword', 'none', 'none', 2)")
cursor.execute("INSERT INTO monsters(name, hd, ac, attack_type, special1, special2, cr) VALUES ('Warrior Ant', 3, 16, 'shortsword', 'poison', 'none', 2)")
cursor.execute("INSERT INTO monsters(name, hd, ac, attack_type, special1, special2, cr) VALUES ('Badger', 3, 16, 'claw', 'bite', 'none', 3)")
cursor.execute("INSERT INTO monsters(name, hd, ac, attack_type, special1, special2, cr) VALUES ('Vampire Bat', 1, 11, 'shortsword', 'drain', 'none', 3)")
cursor.execute("INSERT INTO monsters(name, hd, ac, attack_type, special1, special2, cr) VALUES ('Giant Centipede', 1, 10, 'none', 'poison', 'none', 2)")
cursor.execute("INSERT INTO monsters(name, hd, ac, attack_type, special1, special2, cr) VALUES ('Ghoul', 2, 13, 'bite', 'paralysis', 'none', 3)")

db.commit()

cursor.execute("SELECT * from monsters")

rows = cursor.fetchall()

for row in rows:
    print row

exit()
