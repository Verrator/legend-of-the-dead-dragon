import random
import sqlite3 as lite
import subprocess as shell 
import time
import datetime
import os.path #used for checking if a file exists
import getpass #for working with the password

db = lite.connect('dd.db')
cursor = db.cursor()

cursor.execute("CREATE TABLE IF NOT EXISTS users(ID INTEGER PRIMARY KEY AUTOINCREMENT, name text, password text, str integer, dex integer, con integer, int integer, wis integer, cha integer, hp integer, maxhp integer, save integer, weapon text, armor text, level text, xp integer, xpn integer, gold text, fights integer, lt_d integer, lt_m integer, lt_y integer, status text, magic_item text, pclass text)") #vars1

def intro():
	shell.call("clear")
	print("   .:'                                  `:.")
	print("  ::'                                    `::")
	print(" :: :.                                  .: ::")
	print("  `:. `:.             .             .:'  .:'")
	print("   `::. `::           !           ::' .::'")
	print("       `::.`::.    .' ! `.    .::'.::'")
	print("         `:.  `::::'':!:``::::'   ::'")
	print("         :'*:::.  .:' ! `:.  .:::*`:")
	print("        :: HHH::.   ` ! '   .::HHH ::      Legend of the Dead Dragon:")
	print("       ::: `H TH::.  `!'  .::HT H' :::             v. 1.0")
	print("       ::..  `THHH:`:   :':HHHT'  ..::          Programming by:")
	print("       `::      `T: `. .' :T'      ::'          Terminal Dragon")
	print("         `:. .   :         :   . .:;")
	print("           `::'               `::;              1. Login")
	print("             :'  .`.  .  .'.  `:                2. New Player")
	print("             :' ::.       .:: `:                3. Exit")
	print("             :' `:::     :::' `:")
	print("              `.  ``     ''  .'")
	print("               :`...........':")
	print("               ` :`.     .': ,")
	print("                `:  `...'  : ")
	return


def quit():
	shell.call("clear")
	print("Thanks for playing!")
	print(" ")
	print("              _______. ")
	print("   ______    | .   . |\ ")
	print("  /     /\   |   .   |.\ ")
	print(" /  '  /  \  | .   . |.'| ")
	print("/_____/. . \ |_______|.'| ")
	print("\ . . \    /  \ ' .   \\'| ")
	print(" \ . . \  /    \____'__\| ")
	print("  \_____\/ ")
	print(" ")
	exit()


def special_encounter(name):
	cursor.execute("SELECT * FROM users WHERE name=?", (name,))    #pull in user info from DB
        rows = cursor.fetchall()
	for row in rows:
		str = int(row[3])
	        dex = int(row[4])
		con = int(row[5])
		inte = int(row[6])
		wis = int(row[7])
		cha = int(row[8])
		xp = int(row[15])
		xpn = int(row[16])
		gold = int(row[17])
		fights = int(row[18])
                hp = int(row[9])	
		maxhp = int(row[10])
	encounter = rolldice(1,2)
	if encounter == 1:
		shell.call("clear")
		print("Spooked Horse")
		print("-------------\n\n")
		print("While riding out in the woods your horse gets spooked")
		time.sleep(1)
		event = rolldice(1,20)
		if event <= dex:
			print("However you were able to calm the horse down and continue with no further issue.")
			raw_input("Press enter to continue")
			return
		else:
			damage = rolldice(2,6)
			print("The horse bucks you from its back falling hard to the ground, you suffer %s damage" % (damage))
			time.sleep(2)
			hp = hp - damage
			cursor.execute("UPDATE users SET hp = ? where name = ?", (hp, name))
			db.commit()
			if hp <= 0:
				death(name)
			else:
				raw_input("Press enter to continue")
				return
			
	if encounter == 2:

		shell.call("clear")
		print("Mysterious Fountain")
		print("-------------------\n\n")
		print("You have found a fountain with a strange glowing water deep in the woods:")
		while True:
			choice = raw_input("Do you [D]rink from the fountain or [L]eave? ")
			if choice == "d" or choice == "D":
				fountain = rolldice(1,6)
	
				if fountain == 1:
					time.sleep(1)
					print("You feel yourself becoming more able in some fashion (+1 to stat)")
					stat = rolldice(1,6)
					if stat == 1:
						str = str + 1
						if str > 18:
							str = 18
					elif stat == 2:
						dex = dex + 1
						if dex > 18:
							dex = 18
					elif stat == 3:
						con = con + 1
						if con > 18:
							con = 18
					elif stat == 4:
						inte = inte + 1
						if inte > 18:
							inte = 18
					elif stat == 5:
						wis = wis + 1
						if wis > 18:
							wis = 18
					elif  stat == 6:
						cha = cha + 1
						if cha > 18:
							cha = 18

				if fountain == 2: 
					print("You feel yourself becoming less able in some fashion (-1 to stat)")
					stat = rolldice(1,6)
					if stat == 1:
						str = str - 1
						if str < 1:
							str = 1
					elif stat == 2:
						dex = dex - 1
						if dex < 1:
							dex = 1
					elif stat == 3:
						con = con - 1
						if con < 1:
							con = 1
					elif stat == 4:
						inte = inte - 1
						if inte < 1:
							inte = 1
					elif stat == 5:
						wis = wis - 1
						if wis < 1:
							wis = 1
					elif stat == 6:
						cha = cha - 1
						if cha < 1:
							cha = 1
				if fountain == 3:
					print("On drinking the strange waters you feel great knowledge flow into you (+200 xp)")
					xp = xp + 200
					if xp > xpn:
						level_up(name)
				if fountain == 4:
					print("the water turns to ash in your mouth, you spit it out buts its already too late as some memories fade away (-200 xp)")
					xp = xp - 200
					if xp < 0:
						xp = 0

				if fountain == 5:
					print("the cool water refreshes your body and your mind, you are ready to take on the world (+2 fights)")
					fights = fights + 2

				if fountain == 6:
					print("bleeech! The water tastes of sickness and death, after hurling up what you can you are left nauseated (-2 fights) ")
					fights = fights - 2
					if fights < 0:
						fights = 0
				cursor.execute("UPDATE users SET str = ?, dex = ?, con = ?, int = ?, wis = ?, cha = ?, hp = ?, xp = ?, fights = ? where name = ?", (str, dex, con, inte, wis, cha, hp, xp, fights, name))
				db.commit()
				raw_input("Press enter to continue")
				return



			if choice == "l" or choice == "L":
				raw_input("You decide its best not to drink strange waters found in a forest.")
				raw_input("Press enter to continue")
				return
			else:
				continue
		
	


def explore(name):

        cursor.execute("SELECT * FROM users WHERE name=?", (name,))    #pull in user info from DB
        rows = cursor.fetchall()
        for row in rows:
		fights = int(row[18])
		if fights <=0:
			raw_input("You are too tired to journey any farther today.")
			return
		fights = fights - 1
		cursor.execute("UPDATE users SET fights = ? where name = ?", (fights, name))
		db.commit()

	encounter = rolldice(1,100)
	if encounter >= 85:
		special_encounter(name)
		return

	else:
		# add player level ranges for monsters
		monster = rolldice(1,6)
		combat(name, monster)
		return


def armor_check(armor):
	if armor == "none":
		ac = 10
		return ac
	if armor == "cloth":
		ac = 12
		return ac
	if armor == "leather":
                ac = 13
                return ac
	if armor == "chain":
                ac = 14
                return ac
	if armor == "plate":
                ac = 15
                return ac



def combat(name, monster):

#initialize player variables
        cursor.execute("SELECT * FROM users WHERE name=?", (name,))    #pull in user info from DB
        rows = cursor.fetchall()
        for row in rows:
		dex = int(row[4])
		hp = int(row[9])
		maxhp = int(row[10])
		save = int(row[11])
		weapon = row[12]
		armor = row[13]
		level = int(row[14])
		xp = int(row[15])
		xpn = int(row[16])
                gold = int(row[17])
		status = row[22]
		magic_item = row[23]
                pclass = row[24]
		ac = armor_check(armor)

#initiliaze monster variables
        cursor.execute("SELECT * FROM monsters WHERE id=?", (monster,))    #pull in monster info from DB
        rows = cursor.fetchall()
        for row in rows:
		monster_name = str(row[1])
		monster_hd = int(row[2])
		monster_ac = int(row[3])
		monster_attack = row[4]
		monster_special1 = row[5]
		monster_special2 = row[6]
		monster_cr = int(row[7])
		monster_maxhp = rolldice(monster_hd,8)
		monster_hp = monster_maxhp
		monster_save = 18 - monster_hd
		if monster_save < 3:
			monster_save = 3


	while True:
		shell.call("clear")
		print("Combat!")
		print("=======")
		print(" ")
		print(" ")
		print("Your HP: %s " % (hp) + "/ %s" % (maxhp))
		print("%s HP: %s " % (monster_name, monster_hp) + "/ %s" % (monster_maxhp))
		
		print("monster ac is %s "% (monster_ac))
		combat = raw_input("what do you want to do? [A]ttack or [R]un? ") 
		if combat == "r" or combat == "R":
			run = rolldice(3,6)
			if run <= dex:
				print("You successfully run away!")
				time.sleep(2)
				return
			else:
				print("You have failed to run away!")
				time.sleep(2)
		if combat == "a" or combat == "A":
			player_attack = rolldice (1,20)
			print("you rolled a %s" % (player_attack))
			if player_attack >= monster_ac:
				damage = weapon_check(weapon)
				if player_attack == 20:
					damage = damage * 2
					print("CRITICAL!!!")
				time.sleep(2)
				print("you deal the %s %s damage!" % (monster_name, damage))
				monster_hp = monster_hp - damage
				if monster_hp <=0:
					combat_rewards(name,monster_cr)
					return
			else: 
				if player_attack == 1:
					print("Critical Miss! AC lowered by 2 this turn!")
					ac = ac - 2
				print("you missed with your attack")
		else:
			print("Please use only [A] or [R]")
			time.sleep(2)
			continue
		#monsters turn to attack if combat hasnt came to an end
		monster_attack_roll = rolldice(1,20)
		monster_attack_roll = monster_attack_roll + monster_hd
		if monster_attack_roll >= ac:
			monster_damage = weapon_check(monster_attack)
			print("%s deals %s to you!" % (monster_name, monster_damage))
			time.sleep(2)
			hp = hp - monster_damage
			if player_attack == 1:
				ac = ac + 2 #remove the temporary loss of ac due to critical miss on players part
			cursor.execute("UPDATE users SET hp = ? where name is ? ", (hp, name))
			db.commit()
			if hp <=0:
				death(name)
				return
		else:
			print("%s missed with its attack." % (monster_name))
			time.sleep(2)
			continue
def death(name):

	cursor.execute("SELECT * FROM users WHERE name=?", (name,))    #pull in user info from DB
        rows = cursor.fetchall()
        for row in rows:
                status = row[22]
        cursor.execute("UPDATE users SET status = 'DEAD' where name = ?", (name,))
        db.commit()

	shell.call("clear")

	print("              ...                            ")
	print("             ;::::;                           ")
	print("           ;::::; :;                          ")
	print("         ;:::::'   :;                         ")
	print("        ;:::::;     ;.                        ")
	print("       ,:::::'       ;           OOO\         ")
	print("       ::::::;       ;          OOOOO\        ")
	print("       ;:::::;       ;         OOOOOOOO       ")
	print("      ,;::::::;     ;'         / OOOOOOO      ")
	print("    ;:::::::::`. ,,,;.        /  / DOOOOOO    ")
	print("  .';:::::::::::::::::;,     /  /     DOOOO   ")
	print(" ,::::::;::::::;;;;::::;,   /  /        DOOO  ")
	print(";`::::::`'::::::;;;::::: ,#/  /          DOOO ")
	print(":`:::::::`;::::::;;::: ;::#  /            DOOO")
	print("::`:::::::`;:::::::: ;::::# /              DOO")
	print("`:`:::::::`;:::::: ;::::::#/               DOO")
	print(" :::`:::::::`;; ;:::::::::##                OO")
	print(" ::::`:::::::`;::::::::;:::#                OO")
	print(" `:::::`::::::::::::;'`:;::#                O ")
	print("  `:::::`::::::::;' /  / `:#                ")  
	print("   ::::::`:::::;'  /  /   `#              ")

	print(" ")
	raw_input("It appears death has came for you today, better luck tomorrow.")
	exit()
	

def combat_rewards(name, monster_cr):
	reward_modifier = rolldice(1,3) + 1
        xp_award = monster_xp(monster_cr)	
	gold_reward = xp_award * reward_modifier
	xp_total = xp_award + gold_reward
	cursor.execute("SELECT * FROM users WHERE name=?", (name,))    #pull in user info from DB
        rows = cursor.fetchall()
        for row in rows:
		xp = int(row[15])
		xpn = int(row[16])
		gold = int(row[17])
		xp = xp + xp_total
		gold = gold + gold_reward
	cursor.execute("UPDATE users SET xp = ?, gold = ? where name = ?", (xp, gold, name))
	db.commit()
	print("You recieve %s XP" % (xp_total))
	print("you recieve %s GP" % (gold_reward))
	raw_input("Press enter to continue")
	if xp >= xpn:
		level_up(name)
		raw_input("Press enter to continue")
	return


def level_up(name):
        cursor.execute("SELECT * FROM users WHERE name=?", (name,))    #pull in user info from DB
        rows = cursor.fetchall()
        for row in rows:
		pclass = row[24]
		level = int(row[14])
                xp = int(row[15])
                xpn = int(row[16])
		hp = int(row[9])
		maxhp = int(row[10])
		con = int(row[5])
		save = int(row[11])
	if pclass == "Fighter":
                if level <= 10:
			print("You gained a level!")
                        xpn = xpn * 2
			level = level + 1
			hp_up = rolldice(1,8)
			print("you have gained %s hp" % (hp_up))
			maxhp = maxhp + hp_up
			save = save - 1
        		cursor.execute("UPDATE users SET xpn = ?, level = ?, maxhp = ?, save = ? where name = ?", (xpn, level, maxhp, save,  name))
        		db.commit()
			return
		else:
			return

def weapon_check(weapon):
        if weapon == "none":
                damage = rolldice(1,2)
                return damage
	if weapon == "claw":
		damage = rolldice(1,3)
		return damage
	if weapon == "bite":
		damage = rolldice(1,4)
		return damage
        if weapon == "dagger":
                damage = rolldice(1,4)
                return damage
        if weapon == "shortsword":
                damage = rolldice(1,6)
                return damage
        if weapon == "battle axe":
                damage = rolldice(1,8)
                return damage
        if weapon == "two-handed sword":
                damage = rolldice(2,8)
                return damage
        else:
                print("Something terrible has happened")

def weapon_buy(name):
	cursor.execute("SELECT * FROM users WHERE name=?", (name,))    #pull in user info from DB
        rows = cursor.fetchall()
	for row in rows:
		weapon = row[12]
		gold = int(row[17])
		pclass = row[24]
		if weapon == "none":
			shell.call("clear")
        		print("Weapons")
        		print("=======")
        		print("1. dagger - 50gp")
			print("2. shortsword - 200gp")
			print("3. battle axe - 500gp")
			print("4. two-handed sword - 1500gp\n")
			print(gold)
			print(" ")
			buy = raw_input("What would you like 1-4? ")
			if buy == "1" and gold >= 50:
				weapon = "dagger"
				gold = gold - 50
				print("an excellent choice, enjoy your new dagger")
				raw_input("Press enter to continue")
				cursor.execute("UPDATE users SET weapon = ?, gold = ? where name = ?", (weapon, gold, name))
				db.commit()
				return
		        if buy == "2" and gold >= 200:
                                weapon = "shortsword"
                                gold = gold - 200
                                print("an excellent choice, enjoy your new shortsword")
                                cursor.execute("UPDATE users SET weapon = ?, gold = ? where name = ?", (weapon, gold, name))
                                raw_input("Press enter to continue")
                                db.commit()
                                return
                        if buy == "3" and gold >= 500:
                                weapon = "battle axe"
                                gold = gold - 500
                                print("an excellent choice, enjoy your new battle axe")
                                cursor.execute("UPDATE users SET weapon = ?, gold = ? where name = ?", (weapon, gold, name))
                                raw_input("Press enter to continue")
                                db.commit()
                                return
                        if buy == "4" and gold >= 1500:
                                weapon = "two-handed sword"
                                gold = gold - 1500
                                print("an excellent choice, enjoy your new two-handed sword")
                                cursor.execute("UPDATE users SET weapon = ?, gold = ? where name = ?", (weapon, gold, name))
                                raw_input("Press enter to continue")
                                db.commit()
                                return				
			else:
				print("You dont have enough gold!")
                                raw_input("Press enter to continue")
		else:
			raw_input("You already have a weapon! Press enter to continue")
			return



def armor_buy(name):
        cursor.execute("SELECT * FROM users WHERE name=?", (name,))    #pull in user info from DB
        rows = cursor.fetchall()
        for row in rows:
                armor = row[13]
                gold = int(row[17])
                pclass = row[24]
                if armor == "none":
                        shell.call("clear")
                        print("Armor")
                        print("=======")
                        print("1. cloth - 50gp")
                        print("2. leather - 200gp")
                        print("3. chain - 500gp")
                        print("4. plate - 1500gp\n")
                        print(gold)
                        print(" ")
                        buy = raw_input("What would you like 1-4? ")
                        if buy == "1" and gold >= 50:
                                armor = "cloth"
                                gold = gold - 50
                                print("an excellent choice, enjoy your new cloth armor")
                                raw_input("Press enter to continue")
                                cursor.execute("UPDATE users SET armor = ?, gold = ? where name = ?", (armor, gold, name))
                                db.commit()
                                return
                        if buy == "2" and gold >= 200:
                                armor = "leather"
                                gold = gold - 200
                                print("an excellent choice, enjoy your new leather armor")
                                raw_input("Press enter to continue")
                                cursor.execute("UPDATE users SET armor = ?, gold = ? where name = ?", (armor, gold, name))
                                db.commit()
                                return
                        if buy == "3" and gold >= 500:
                                armor = "chain"
                                gold = gold - 500
                                print("an excellent choice, enjoy your new chain armor")
                                raw_input("Press enter to continue")
                                cursor.execute("UPDATE users SET armor = ?, gold = ? where name = ?", (armor, gold, name))
                                db.commit()
                                return
                        if buy == "4" and gold >= 1500:
                                armor = "plate"
                                gold = gold - 1500
                                print("an excellent choice, enjoy your new plate armor")
                                raw_input("Press enter to continue")
                                cursor.execute("UPDATE users SET armor = ?, gold = ? where name = ?", (armor, gold, name))
                                db.commit()
                                return

                        else:
                                print("You dont have enough gold!")
                                raw_input("Press enter to continue")
                else:
                        raw_input("You already have some armor! Press enter to continue")
                        return


def blacksmith_sell(name):
        cursor.execute("SELECT * FROM users WHERE name=?", (name,))    #pull in user info from DB
        rows = cursor.fetchall()
        for row in rows:
		weapon = row[12]
                armor = row[13]
                gold = int(row[17])
                pclass = row[24]
	select = raw_input("You selling a [W]eapon or [A]rmor? ")
	if select == "w" or select == "W" and weapon != "none":
		if weapon == "none":
			raw_input("You dont have a weapon to sell! Press enter to continue")
			return
		if weapon == "dagger":
			print("I will give you 25 gp for that")
			value = 25
                if weapon == "shortsword":
                        print("I will give you 100 gp for that")
                        value = 100
                if weapon == "battle axe":
                        print("I will give you 250 gp for that")
                        value = 250
                if weapon == "two-handed sword":
                        print("I will give you 750 gp for that")
                        value = 750

		accept = raw_input("[Y]es or [N]o? ")
		if accept == "y" or accept == "Y":
			gold = gold + value
			weapon = "none"
                        cursor.execute("UPDATE users SET weapon = ?, gold = ? where name = ?", (weapon, gold, name))
                        db.commit()
			raw_input("A pleasure doing business with you. Press enter to continue")
			return
		if accept == "n" or accept == "N":
			raw_input("Perhaps another time then. Press enter to continue")
			return
		else:
			raw_input("Dont be wasting my time here. Press enter to continue")
			return

        if select == "a" or select == "A" and weapon != "none":
		if armor == "none":
                        raw_input("You dont have any armor to sell! Press enter to continue")
			return
                if armor == "cloth":
                        print("I will give you 25 gp for that")
                        value = 25
                if armor == "leather":
                        print("I will give you 100 gp for that")
                        value = 100
                if armor == "chain":
                        print("I will give you 250 gp for that")
                        value = 250
                if armor == "plate":
                        print("I will give you 750 gp for that")
                        value = 750

                accept = raw_input("[Y]es or [N]o? ")
                if accept == "y" or accept == "Y":
                	gold = gold + value
                        armor = "none"
                        cursor.execute("UPDATE users SET armor = ?, gold = ? where name = ?", (armor, gold, name))
                        db.commit()
                        raw_input("A pleasure doing business with you. Press enter to continue")
                        return
                if accept == "n" or accept == "N":
                        raw_input("Perhaps another time then. Press enter to continue")
                        return
                else:
                        raw_input("Dont be wasting my time here. Press enter to continue")
                        return
	else:
		raw_input("Dont be wasting my time then. Press enter to continue")
                return
def tavern(name):

	cursor.execute("SELECT * FROM users WHERE name=?", (name, ))#pull in user info from DB
	rows = cursor.fetchall()
	for row in rows:
		str = int(row[3])
		dex = int(row[4])
		con = int(row[4])
		hp = int(row[9])
		maxhp = int(row[10])
		save = int(row[11])
		xp = int(row[15])
		xpn = int(row[16])
		gold = int(row[17])
		pclass = row[24]

	shell.call("clear")

	print("                           (   )")
	print("                          (    )")
	print("                           (    )")
	print("                          (    )")
	print("                            )  )")
	print("                           (  (                  /\ ")
	print("                            (_)                 /  \  /\ ")
	print("                    ________[_]________      /\/    \/  \ ")
	print("           /\      /\        ______    \    /   /\/\  /\/\ ")
	print("          /  \    //_\       \    /\    \  /\/\/    \/    \ ")
	print("   /\    / /\/\  //___\       \__/  \    \/")
	print("  /  \  /\/    \//_____\       \ |[]|     \ ")
	print(" /\/\/\/       //_______\       \|__|      \    1. talk to bartender")
	print("/      \      /XXXXXXXXXX\                  \   2. listen to gossip")
	print("        \    /_I_II  I__I_\__________________\  3. listen to bard")
	print("               I_I|  I__I_____[]_|_[]_____I     4. leave")
	print("               I_II  I__I_____[]_|_[]_____I")
	print("               I II__I  I     XXXXXXX     I")
	print("            ~~~~~"   "~~~~~~~~~~~~~~~~~~~~~~~~")

	while True:
		choice = raw_input("Select [1],[2],[3],[4] ")

		if choice == "1":

			bartender= raw_input("What can I getcha, [F]ood or [I]nformation? ")
			if bartender == "f" or bartender == "F":
				food = raw_input("A hot meal will cost you 25gp, [Y] or [N]o? ")
			if food == "y" or food == "Y" and gold >= 25:
				raw_input("You get a good hot meal and regain 20hp.")
				hp = hp + 20
				if hp > maxhp:
					hp = maxhp
				gold = gold - 25
				cursor.execute("UPDATE users SET hp = ?, gold = ? where name = ?", (hp, gold, name))
				db.commit()
				return
			elif food == "y" or food == "Y" and gold > 25:
				raw_input("I dont run a charity here, go get some more gold!")
				return
		
			elif bartender == "i" or bartender == "I": 
				raw_input("I will let ya know when I know something")
			else:
				continue
		if choice == "2":
			raw_input("No one has any really good gossip at the moment")
			return

		if choice == "3":
			raw_input("He just plays a simple tune, he appears to still be warming up")
			return

		if choice == "4":
			return

		else:
			continue


def blacksmith(name):

	while True:
	        shell.call("clear")

		print("Blacksmith\n")
		print("       |")
		print("       /~.")
		print("Oxxxxx|  (|=========================-")
		print(" \____/\_/")
		print("       |")
		print(" ")
		select = raw_input("Are you here to [B]uy or [S]ell or [L]eave? " )
		if select == "b" or select == "B":
			weapon_armor = raw_input("You in the market for [W]eapons or [A]rmor? " )
			if weapon_armor == "w" or weapon_armor == "W":
				weapon_buy(name)
				continue
			if weapon_armor == "a" or weapon_armor == "A":
				armor_buy(name)
				continue
			else:
				raw_input("Stop wasting my time, I got a business to run. Press enter to continue")
				return			

		if select == "s" or select == "S":
			blacksmith_sell(name)
			continue

		else:
                	raw_input("Stop wasting my time, I got a business to run. Press enter to continue")
                	return


def church(name):
	shell.call("clear")

	print("                  _|_ ")
	print("                   | ")
	print("                  / \ ")
	print("                 //_\\ ")
	print("                //(_)\\ ")
	print("                 |/^\|  ")
	print("       ,%%%%     // \\    ,@@@@@@@, ")
	print("     ,%%%%/%%%  //   \\ ,@@@\@@@@/@@, ")
	print(" @@@%%%\%%//%%%// === \\ @@\@@@/@@@@@ ")
	print("@@@@%%%%\%%%%%// =-=-= \\@@@@\@@@@@@;%#####, ")
	print("@@@@%%%\%%/%%//   ===   \\@@@@@@/@@@%%%######, ")
	print("@@@@@%%%%/%%//|         |\\@\\//@@%%%%%%#/#### ")
	print("'@@@@@%%\\/%~ |         | ~ @|| %\\//%%%#####; ")
	print("  @@\\//@||   |  __ __  |    || %%||%%'###### ")
	print("   '@||  ||   | |  |  | |    ||   ||##\//#### ")
	print("     ||  ||   | | -|- | |    ||   ||'#||###' ")
	print("     ||  ||   |_|__|__|_|    ||   ||  ||    ")
	print("     ||  ||_/`  =======  `\__||_._||  ||    ")
	print("   __||_/`      =======            `\_||___")	
	print(" ")
	choice = raw_input("What can I do for you child? [H]ealing or [L]eave ")
	if choice == "h" or choice == "H":
		print("That will be a 100gp donation")
		agree = raw_input("Do you agree [Y] or [N]o ")
		if agree == "y" or agree == "Y":		

	        	cursor.execute("SELECT * FROM users WHERE name=?", (name, ))    #pull in user info from DB
        		rows = cursor.fetchall()

        		for row in rows:
                		hp = row[9]
                		maxhp = row[10]
				gold = int(row[17])
			if gold <=100:
				raw_input("My apologies child but we require your charity to perform miracles")
				return
			else:
				hp = maxhp
				gold = gold - 100
				cursor.execute("UPDATE users SET hp = ?, gold = ? where name = ?", (hp, gold, name))
				db.commit()
				raw_input("The clergy man bestows a blessing on you and you are fully healed!")	
		else:
			raw_input("Very well then, may the gods smile blessings upon you and your travels.")
			return
	else:
        	raw_input("We thank you for visiting today.")
                return


def date_check(name):

        cursor.execute("SELECT * FROM users WHERE name=?", (name, ))    #pull in user info from DB
        rows = cursor.fetchall()

        for row in rows:
		hp = row[9]
		maxhp = row[10]
		status = row[22]
        	lt_d = row[19]
		lt_m = row[20]
		lt_y = row[21]

	day = datetime.datetime.now().day
        month = datetime.datetime.now().month
        year = datetime.datetime.now().year

	if day > lt_d or month > lt_m or year > lt_y:
		fights = 15
		if hp<=0:
			hp = maxhp
		if status == "DEAD":
			status = "ALIVE"
		cursor.execute("UPDATE users SET fights = ?, lt_d = ?, lt_m = ?, lt_y = ?, hp = ?, status = ? where name = ?", (fights, day, month, year, hp, status, name))
		db.commit()
		print("You feel refreshed and ready to take on the day!")
                raw_input("Press enter to continue")
		return

	if day == lt_d and month == lt_m and year == lt_y and hp <= 0:
		print("You have already died today, return tomorrow!")
		exit()

	else:
		print("ah so you have returned again today...")
		raw_input("Press enter to continue")
		return
	

def monster_xp(monster_cr):

        xp = 15
        for i in range(1, monster_cr):
                xp = xp * 2
        return xp

#dice roller for all random outcomes
def rolldice(numberofdice, maxvalue):
        rolledvalue = 0
        for i in range(0,numberofdice):
                rolledvalue += random.randint(1,maxvalue)
        return rolledvalue



def login(name):

	while True:
		cursor.execute("SELECT * FROM users WHERE name=?", (name, ))    #pull in user info from DB
	        rows = cursor.fetchone()
		if rows is None:

	                print("User does not exist")
	                time.sleep(2)
	                return
	
		else:
			cursor.execute("SELECT * FROM users WHERE name=?", (name, ))    #pull in user info from DB
        		rows = cursor.fetchall()

			for row in rows:
				password_confirm = str(row[2])
                	

			password = getpass.getpass("Please enter a password: ")

                	if password != password_confirm:
                        	print("Passwords do not match")
                        	time.sleep(2)
                        	shell.call("clear")
				continue
			
			elif password == password_confirm:
				date_check(name)
				main(name)
				return

def view_char(name):
	shell.call("clear")
	cursor = db.cursor()
	cursor.execute("SELECT * FROM users WHERE name=?", (name, ))#pull in user info from DB
	rows = cursor.fetchall()
        for row in rows:
	        print("Character name: %s" % row[1])
       		print("Stats:")
               	print("Level: %s" % row[14])
               	print("HP: %s " % row[9] + "        Max HP: %s" % row[10])
               	print("Exp: %s " % row[15] + "      Next Level: %s" % row[16])
               	print("STR: %s " % row[3] + "       Weapon: %s" % row[12])
               	print("DEX: %s " % row[4] + "       Armor: %s" % row[13])
               	print("CON: %s " % row[5] + "       Save: %s" % row[11])
               	print("INT: %s " % row[6] + "       Gold: %s" % row[17])
               	print("WIS: %s " % row[7] + "       Fights Remaining: %s" % row[18])
               	print("CHA: %s " % row[8] )
		raw_input("Press enter to continue")
		return

def new_user():
	weapon = "none"
	armor = "none"
	level = 1
	hp = 10
	xp = 0
	gold = 50
	fights = 15
	lt_d = datetime.datetime.now().day
	lt_m = datetime.datetime.now().month
	lt_y = datetime.datetime.now().year
	status = "ALIVE"
	magic_item = "none"

	while True:
		shell.call("clear")
		cursor = db.cursor()
		name = raw_input("Alright what shall we call you then? ")
		if cursor.execute("SELECT * FROM users WHERE name=?", (name, )).fetchone() is not None: #doing checks for current username
                	print("User with that name already exists")
                	raw_input("Press enter to continue")
		else:
			break

	while True:
		password = getpass.getpass("Please enter a password: ")
		password_confirm = getpass.getpass("Please confirm password: ")

		if password != password_confirm:
			print("Passwords do not match")
			time.sleep(2)
			shell.call("clear")
	
		elif password == password_confirm:
			print("Excellent rolling stats: ")
			break
		


	if cursor.execute("SELECT * FROM users WHERE name=?", (name, )).fetchone() is None: #check for users with same name
		str = rolldice(3,6)
		dex = rolldice(3,6)
		con = rolldice(3,6)
		int = rolldice(3,6)
		wis = rolldice(3,6)
		cha = rolldice(3,6)
		shell.call("clear")
		print("Your stats are:")
		print(" ")
		print("STR: %s" % str)
               	print("DEX: %s" % dex)
               	print("CON: %s" % con)
               	print("INT: %s" % int)
               	print("WIS: %s" % wis)
               	print("CHA: %s" % cha)
		while True:

			class_select = raw_input("Select your class, [F]ighter,[M]age,[T]hief,[C]leric ")		
			if class_select == "f" or class_select == "F":
					pclass = 'Fighter'
					hp = hp + rolldice(1,8)
					maxhp = hp
					if con >= 13:
						hp = hp + 1
						maxhp = hp
					if con <= 8 and hp >= 2:
						hp = hp - 1
						maxhp = hp
					save = 14
					xpn = 2000
					cursor.execute("INSERT INTO users(name, password, str, dex, con, int, wis, cha, hp, maxhp, save, weapon, armor, level, xp, xpn, gold, fights, lt_d, lt_m, lt_y, status, magic_item, pclass) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", (name, password, str, dex, con, int, wis, cha, hp, maxhp, save, weapon, armor, level, xp, xpn, gold, fights, lt_d, lt_m, lt_y, status, magic_item, pclass))#vars2
					db.commit()
					main(name)

def start():
	while True:
		intro()
		print(" ")
		print(" ")

		selection = raw_input("Select [1],[2],[3]: ")
		if selection == "1":
			name = raw_input("please enter your name: ")
			login(name)
			return
		elif selection == "2":
			new_user()
			return
		elif selection == "3":
			quit()
		


def main(name):
        while True:
		shell.call("clear")
                print("City of Beltaine ")
                print(" ")
		print(" ")
		print("~         ~~          __")
		print("       _T      .,,.    ~--~ ^^")
		print(" ^^   // \                    ~")
		print("      ][O]    ^^      ,-~ ~")
		print("   /''-I_I         _II____")
		print("__/_  /   \ ______/ ''   /'\_,__   1. City Gate")
		print("  | II--'''' \,--:--..,_/,.-{ },   2. Tavern")
		print("; '/__\,.--';|   |[] .-.| O{ _ }   3. Blacksmith")
		print(":' |  | []  -|   ''--:.;[,.'\,/    4. Church")
		print("'  |[]|,.--'' '',   ''-,.    |     5. Stats")
		print("  ..    ..-''    ;       ''. '     6. Exit")
		print(" ")
                selection = raw_input("Select [1],[2],[3],[4],[5],[6]: ")
                if selection == "1":
			explore(name)
                elif selection == "2":
                        tavern(name)
                elif selection == "3":
                        blacksmith(name)
		elif selection == "4":
			church(name)
		elif selection == "5":
			view_char(name)
		elif selection == "6":
			quit()
start()
